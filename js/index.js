let firstNum = parseFloat(prompt('Enter first number'));
    while (isNaN(firstNum)) {
        firstNum = parseFloat(prompt('Enter first number'));
    }

let operation = prompt('Enter the operation sign');
    while (operation !== '+'
        && operation !== '-'
        && operation !== '*'
        && operation !== '/') {
    operation = prompt('Enter the operation sign');
}

let secondNum = parseFloat(prompt('Enter second number'));

while (isNaN(secondNum)) {
    secondNum = parseFloat(prompt('Enter second number'));
}

console.log(calculate(firstNum, operation, secondNum));

function calculate(firstNum, operation, secondNum) {
    let calc = 0;

    switch (operation) {
        case '+':
            calc = firstNum + secondNum;
            break;
        case '-':
            calc = firstNum - secondNum;
            break;
        case '*':
            calc = firstNum * secondNum;
            break;
        case '/':
            calc = firstNum / secondNum;
            break;
    }

    return calc;
}
